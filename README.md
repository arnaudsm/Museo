# Museo
Projet 4A EPF Android

## Authors
- [Arnaud de Saint Meloir](https://arnaud.at/)
- [Ferdinand  Ferdinand Duchet-Suchaux](https://github.com/fduchet)

# Documentation

## Consulter les Musées
La page d'accueil de l'application vous présente tous les Musées sauvés en mémoire.
![Video](demo/videos/list_full.mp4)

## Scanner un Musée 
Appuyer sur l'icône de Scan pour Scanner le QR code d'un Musée.
![Video](demo/videos/scan.mp4)

## Consulter un Musée 
Cliquer sur la carte d'un musée pour en visualiser les informations.
![Video](demo/videos/fiche.mp4)

## Ajouter une photo
Cliquez sur l'icône Caméra pour ajouter une photo à un Musée.
![Video](demo/videos/new_pic.mp4)


## **Bonus** : Localiser le Musée
Le bouton Carte permet de visualiser le Musée dans **Google Maps** pour s'y rendre. 
![Video](demo/videos/map.mp4)


# Example QR Codes
![Code Invalide](/demo/qr/qr_arnaud.png)
Code Invalide

![Code Valide](/demo/qr/qr_musee.png)
Code Valide


# Todo
- [x] Scanner un QR Code
- [x] Vérifier si un QR Code est valide
- [x] Vérifier si un musée est téléchargé
- [x] Télécharger un musée en local
- [x] Afficher un musée
- [x] Télécharger et visualiser les photos associées à un musée
- [x] Ajouter des photos à la fiche d’un musée,
- [x] Lister les musées téléchagés dans l'onglet principal
- [x] Documentation utilisateur